package com.exchangelodge.tester;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.jaxrs.client.ClientConfiguration;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.ContentDisposition;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;
import org.apache.cxf.jaxrs.provider.jsrjsonp.JsrJsonpProvider;
import org.apache.cxf.transport.http.HTTPConduit;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonException;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Stack;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Application {


    private static class TrustServerCertificatesTrustManager implements X509TrustManager {

        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }
    }

    private static class RuleInfo {

        public String name;
        public String pkg;
        public String version;
    }

    public static String appVersion = "2.0";

    public static void main(String[] args) {

        Map<String, String> arguments = new HashMap<>();
        List<RuleInfo> ruleNames = new ArrayList<>();

        System.out.println("ExchangeLodge Automated Deployer " + appVersion);

        try {

            // Read arguments
            String swName = null;
            for (String arg : args) {
                if (arg.startsWith("--"))
                    swName = arg.substring(2);
                else {
                    if (StringUtils.isEmpty(swName)) {
                        System.err.printf("Invalid syntax.\n");
                        return;
                    }
                    arguments.put(swName, arg);
                    swName = null;
                }
            }
            String action = arguments.getOrDefault("action", "deploy");
            boolean purgeRules = false;
            if (action.equals("purge|rules")) {
                action = "purge";
                purgeRules = true;
            }

            // Get authentication values
            final String basicAuth = arguments.getOrDefault("basicAuth", null);
            if (!arguments.containsKey("key") && StringUtils.isEmpty(basicAuth)) {
                System.err.printf("Missing Consumer key.\n");
                return;
            }
            if (!arguments.containsKey("secret") && StringUtils.isEmpty(basicAuth)) {
                System.err.printf("Missing Consumer secret.\n");
                return;
            }
            final String apiUrl = arguments.getOrDefault("apiUrl", "https://api.vagrant.local:1443/");
            System.out.printf("Using API manager URL: %s.\n", apiUrl);
            // If we have Basic Auth bearer, just use it.... otherwise, use keys to get one.
            String authBearer;
            if (StringUtils.isEmpty(basicAuth))
                authBearer = "Bearer " + getApplicationToken(apiUrl, arguments.get("key"), arguments.get("secret"));
            else
                authBearer = "Bearer " + getApplicationToken(apiUrl, basicAuth);

            final String api = "exl/1.0/";

            final String tenantId = arguments.getOrDefault("tenant", null);
            if (StringUtils.isEmpty(tenantId)) {
                System.err.printf("Missing tenant in argument list.\n");
                return;
            }

            final String packageFile = arguments.getOrDefault("package", null);
            if (StringUtils.isEmpty(packageFile)) {
                System.err.printf("Missing package path in arguments.\n");
                return;
            }
            final String packageName = Paths.get(packageFile).getFileName().toString();
            String description;
            String uuid;
            String name;
            String version;

            // Read information from package.
            java.nio.file.Path path = null;
            File testFile = new File(packageFile);
            if (!testFile.exists()) {
                System.err.printf("Could not find package file: %s\n", testFile.getAbsolutePath());
                return;
            }
            try {
                System.out.printf("Verifying package '%s'...\n", testFile.getAbsolutePath());
                // File should be a ZIP. Create temp folder, extract and get descriptor data to deploy
                File baseDir = new File(System.getProperty("java.io.tmpdir"));
                UUID directoryId = UUID.randomUUID();
                path = Paths.get(String.format("%s/%s", baseDir.getAbsolutePath(), directoryId));
                Files.createDirectory(path);
                String filePath = path.toString();
                System.out.printf("Extracting package contents to temp dir: '%s'...\n", filePath);
                extractFileContents(testFile, filePath);
                System.out.printf("Verifying package contents...\n");
                // Get descriptor
                Document descriptor = readDescriptor(filePath);
                // Get important information from descriptor
                // Look for standard nodes...
                NodeList list = descriptor.getElementsByTagName("business-event");
                if (list.getLength() != 1)
                    throw new RuntimeException("Invalid descriptor file. Missing business-event node.");
                // Get node
                Node node = list.item(0);
                // Package name
                if (StringUtils.isEmpty(name = getNodeAttr(node, "name")))
                    if (StringUtils.isEmpty(name = arguments.getOrDefault("name", null))) {
                        System.err.printf("Could not find Business Event name in package/argument list.\n");
                        return;
                    }
                // Package UUID
                if (StringUtils.isEmpty(uuid = getNodeAttr(node, "uniqueId")))
                    if (StringUtils.isEmpty(uuid = arguments.getOrDefault("uuid", null))) {
                        System.err.printf("Missing UUID in package/argument list.\n");
                        return;
                    }
                // Package description
                if (StringUtils.isEmpty(description = getNodeData(node, "description")))
                    if (StringUtils.isEmpty(description = name))
                        if (StringUtils.isEmpty(description = arguments.getOrDefault("description", null))) {
                            System.err.printf("Could not get description from package/ command line.\n");
                            return;
                        }
                // Package version
                if (StringUtils.isEmpty(version = getNodeData(node, "version")))
                    if (StringUtils.isEmpty(version = arguments.getOrDefault("version", null))) {
                        System.err.printf(Locale.US, "Could not get version from package/command line.\n");
                        return;
                    }
                // Read rule definitions and store names. If they ask us to purge rules, we do
                list = descriptor.getElementsByTagName("rules");
                for (int i = 0; i < list.getLength(); i++) {
                    Node rn = list.item(i);
                    NodeList rdlist = rn.getChildNodes();
                    for (int j = 0; j < rdlist.getLength(); j++) {
                        Node rd = rdlist.item(j);
                        String nodeName = rd.getNodeName();
                        if (StringUtils.isNotEmpty(nodeName) && nodeName.equals("rule")) {
                            RuleInfo info = new RuleInfo();
                            info.name = getNodeAttr(rd, "name");
                            info.pkg = getNodeAttr(rd, "package");
                            info.version = StringUtils.defaultIfEmpty(getNodeAttr(rd, "version"), version);
                            ruleNames.add(info);
                        }
                    }
                }

            }
            catch (IOException e) {
                System.err.printf(Locale.US, "I/O error reading package file\n", e.toString());
                return;
            }
            finally {
                // Cleanup intermediate data... We don't needed anymore...
                cleanup(path);
            }

            JsonObject definition = null;
            try {
                definition = getBusinessEventDefinition(tenantId, apiUrl, api, authBearer, uuid, version);

                if (action.equals("deploy") && definition != null) {
                    System.err.printf("There's a Business Event with this UUID/version already deployed for this tenant.\n");
                    return;
                }
                if ((action.equals("undeploy") || action.equals("purge")) && definition == null) {
                    // Verify no package with this UUID
                    if (null == getBusinessEventPackage(tenantId, apiUrl, api, authBearer, uuid, version)) {
                        System.err.printf("There's no Business Event with this UUID/version. No action needed.\n");
                        return;
                    }
                }
            }
            catch (Exception e) {
                // Eat it up... assume we're good.
            }
            WebClient client = WebClient.create(apiUrl, new ArrayList<Object>() {{
                add(new JsrJsonpProvider());
                add(new JacksonJsonProvider());
            }});
            client = client.path(api).path("businessEventPackage");
            client.accept(MediaType.APPLICATION_JSON);
            // get web client configuration
            configureClient(client);
            client.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, authBearer);
            client.header("X-Tenant-Id", tenantId);
            client.query("filter", "identifier eq '" + uuid + "' and version eq '" + version + "'");
            Response response = client.get();
            JsonObject jresult = response.readEntity(JsonObject.class);
            System.out.printf("Result: %d  %s \n", response.getStatus(), jresult.toString());

            if (action.equals("deploy")) {
                // Upload package file
                try {
                    if (null == (jresult = uploadPackage(tenantId, apiUrl, api, authBearer, packageFile)))
                        return;
                    System.out.printf("Uploaded Package result: \n%s\n", jresult.toString());
                    JsonObjectBuilder cnt = Json.createObjectBuilder();
                    jresult.entrySet().forEach(entry -> cnt.add(entry.getKey(), entry.getValue()));
                    // This is the information related to the package upload. Create a request to deploy this package and check results
                    JsonObject jdeploy = Json.createObjectBuilder().add("$class", "BusinessEventPackage").add("identifier", uuid)
                        .add("name", name).add("filename", packageName).add("status", "Pending").add("loaded", true).add("version", version)
                        .add("content", Json.createObjectBuilder().add("$class", "Document").add("name", "Package").add("description", description)
                            .add("type", "Package").add("status", "Pending").add("content", cnt)).build();
                    // Call the deploy API
                    JsonObject jdresult = deployPackage(tenantId, apiUrl, api, authBearer, jdeploy);
                    if (jdresult == null) {
                        System.out.printf("Error deploying package.");
                        return;
                    }
                    System.out.printf("Package deployed. Response:\n%s\n", jdresult.toString());
                    // Wait till deployment status is definitive
                    String deploymentId = jdresult.getString("id");
                    System.out.printf("Package deployed with ID: %s\n Package Status: %s\n", deploymentId, jdresult.getJsonObject("status").getString("value"));
                    jdresult = waitForPackageStatus(tenantId, apiUrl, api, authBearer, jdresult);
                    System.out.printf("Package deployment completed. Status:\n%s\n", jdresult.toString());
                    // Print results
                    String status;
                    System.out.printf("Deployment status: %s\n", status = jdresult.getJsonObject("status").getString("value"));
                    if (status.equals("Success")) {
                        // If we have Business Event, get it...
                        JsonObject businessEvent = getBusinessEventDefinition(tenantId, apiUrl, api, authBearer, uuid, version);
                        System.out.printf("Business Event Definition deployed. Result:\n%s\n", businessEvent.toString());
                        String activitiId;
                        System.out.printf("Deployed Business Event Definition ID: %s\nWith Process: %s\nActiviti Deployment ID: %s\n",
                            businessEvent.getString("id"), businessEvent.getJsonObject("process").getJsonObject("document").getString("name"),
                            activitiId = businessEvent.getJsonObject("process").getString("deploymentId"));
                    }
                    else {
                        System.out.printf("There was an error deploying the Business Event Package.\n");
                        String error = jdresult.getString("statusDetails");
                        if (!StringUtils.isEmpty(error))
                            System.out.printf("Deployer returned error: %s\nCheck logs for details.\n", error);
                    }
                }
                catch (Exception e) {
                    System.out.printf("Error running calls: %s\n", e.getMessage());
                }
            }
            else if (action.equals("undeploy") || action.equals("purge") || action.equals("clean")) {
                // Delete Business Event Definition
                try {
                    // We may have package but NOT definition (package deployment may have failed)
                    if (((action.equals("purge") || action.equals("clean"))) && definition != null)
                        purgeBusinessEventInstances(tenantId, apiUrl, api, authBearer, definition.getString("id"));
                    if (action.equals("undeploy") || action.equals("purge")) {
                        if (definition != null) {
                            System.out.printf("Deleting Business Event Definition %s...\n", definition.getString("id"));
                            deleteBusinessEventDefinition(tenantId, apiUrl, api, authBearer, definition.getString("id"));
                        }
                        else
                            System.out.printf("No Business Event Definition found. No need to delete definitions.\n");
                        System.out.printf("Deleting Business Event Package with UUID: '%s' Version (%s)...\n", uuid, version);
                        deleteBusinessEventPackage(tenantId, apiUrl, api, authBearer, uuid, version);
                        // If the action is purge and we have to undeploy rules, do it
                        if (purgeRules) {
                            for (RuleInfo ri : ruleNames) {
                                System.out.printf(Locale.US, "Trying to undeploy rule %s.%s v%s...\n", ri.pkg, ri.name, ri.version);
                                client = WebClient.create(apiUrl, new ArrayList<Object>() {{
                                    add(new JsrJsonpProvider());
                                    add(new JacksonJsonProvider());
                                }});
                                client = client.path("rules/2.0/ruleSet");
                                configureClient(client);
                                client.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                                    .header(HttpHeaders.AUTHORIZATION, authBearer).header("X-Tenant-Id", tenantId);
                                client.type(MediaType.APPLICATION_JSON);
                                client.query("filter", "package eq '" + ri.pkg + "' and name eq '" + ri.name + "' and version eq '" + ri.version + "'");
                                response = client.get();
                                if (response.getStatus() != 200) {
                                    System.err.printf(Locale.US, "An error occurred getting ruleSet %s.%s v%s.\n", ri.pkg, ri.name, ri.version);
                                    throw new Exception("Error getting ruleSet:" + response.readEntity(String.class));
                                }
                                JsonObject result = response.readEntity(JsonObject.class);
                                if (result.getJsonArray("value").size() == 1) {
                                    JsonObject rule = result.getJsonArray("value").getJsonObject(0);
                                    client = WebClient.create(apiUrl, new ArrayList<Object>() {{
                                        add(new JsrJsonpProvider());
                                        add(new JacksonJsonProvider());
                                    }});
                                    client = client.path("rules/2.0/ruleSet").path(rule.getString("id"));
                                    configureClient(client);
                                    client.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                                        .header(HttpHeaders.AUTHORIZATION, authBearer).header("X-Tenant-Id", tenantId);
                                    client.type(MediaType.APPLICATION_JSON);
                                    response = client.delete();
                                    if (response.getStatus() != 200 && response.getStatus() != 204) {
                                        // log information
                                        System.err.printf(Locale.US, "An error occurred deleting ruleSet [%s].\n", rule.getString("id"));
                                        // we received an error status from the server, throw an internal server exception
                                        throw new Exception("Error getting ruleSet:" + response.readEntity(String.class));
                                    }
                                }

                            }
                        }
                    }
                    System.out.printf("Success.\n");
                }
                catch (Exception e) {
                    System.err.printf("Error undeploying package: %s\n", e.getMessage());
                    return;
                }
            }
        }

        catch (JsonException e) {
            System.out.printf(Locale.US, "Error: %s\n", e.getMessage());
        }
    }

    /**
     * Try to delete files and directory.
     */
    private static void cleanup(Path path) {
        System.out.printf("Cleaning up temp directory: %s...\n", path.toString());
        if (!StringUtils.isEmpty(path.toString())) {
            File dir = new File(path.toString());
            File[] currList;
            Stack<File> stack = new Stack<>();
            stack.push(dir);
            while (!stack.isEmpty()) {
                if (stack.lastElement().isDirectory()) {
                    currList = stack.lastElement().listFiles();
                    assert currList != null;
                    if (currList.length > 0) {
                        for (File curr : currList)
                            stack.push(curr);
                    }
                    else
                        stack.pop().delete();
                }
                else
                    stack.pop().delete();
            }
        }
    }

    private static Document readDescriptor(String filePath) throws IOException {
        File file = new File(String.format("%s/descriptor.xml", filePath));
        try (InputStream inputStream = new FileInputStream(file)) {
            Reader reader = new InputStreamReader(inputStream, "UTF-8");
            InputSource inputSource = new InputSource(reader);
            inputSource.setEncoding("UTF-8");
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(false);
            factory.setValidating(false);
            return factory.newDocumentBuilder().parse(inputSource);
        }
        catch (FileNotFoundException e) {
            System.err.printf("Could not find file: %s\n", e.getMessage());
            throw e;
        }
        catch (IOException e) {
            System.err.printf("Error accessing file: %s\n" + e.getMessage());
            throw e;
        }
        catch (SAXException | ParserConfigurationException e) {
            System.err.printf("Error parsing descriptor file: %s %s\n", file, e.getMessage());
            throw new RuntimeException("Error parsing descriptor.");
        }
    }

    private static void extractFileContents(File testFile, String path) throws IOException {
        // Read into ZIP stream
        ZipInputStream zinput = new ZipInputStream(new FileInputStream(testFile));
        ZipEntry entry;
        while (null != (entry = zinput.getNextEntry())) {
            // If it's a directory, we should create it... or maybe not :-P
            String name = entry.getName();
            File file = new File(String.format("%s/%s", path, name));
            if (name.endsWith("/")) {
                if (!file.mkdirs())
                    throw new IOException("Error creating " + file.getAbsolutePath());
                continue;
            }
            // Check if the upper structure is created already
            File parent = file.getParentFile();
            if (parent != null) {
                boolean result = parent.mkdirs();
                if (!result)
                    System.out.printf(".");
            }
            // If we're here, it's a file, the directory is already created, and we can
            byte[] buffer = new byte[4096];
            try (FileOutputStream stream = new FileOutputStream(String.format("%s/%s", path, name))) {
                while (zinput.available() > 0) {
                    int read = zinput.read(buffer, 0, buffer.length);
                    if (read >= 0)
                        stream.write(buffer, 0, read);
                }
            }
        }
        System.out.printf(".\n");
    }

    private static String getNodeData(Node source, String name) {
        Objects.requireNonNull(source, "source cannot be null.");
        Objects.requireNonNull(name, "name cannot be null.");

        NodeList list;
        Element element = (Element)source;

        list = element.getElementsByTagName(name);
        if (list.getLength() == 0)
            return null;
        Node node = list.item(0);
        return node.getTextContent();
    }

    private static String getNodeAttr(Node node, String attr) {
        Objects.requireNonNull(node, "node cannot be null.");
        Objects.requireNonNull(attr, "attr cannot be null.");

        Node element = node.getAttributes().getNamedItem(attr);
        if (element == null)
            return null;
        return element.getNodeValue();
    }


    private static void purgeBusinessEventInstances(String tenantId, String apiUrl, String exlUrl, String authBearer, String definitionId) throws Exception {
        JsonObject list = resolveObjectReferences(getBusinessEventInstances(tenantId, apiUrl, exlUrl, authBearer, definitionId));
        JsonArray instances = list.getJsonArray("value");
        instances.getValuesAs(JsonObject.class).forEach(instance -> {
            System.out.printf("Deleting Business Event instance: %s...\n", instance.getString("id"));
            try {
                if (instance.getJsonObject("status").getString("value").equals("Running"))
                    stopBusinessEventInstance(tenantId, apiUrl, exlUrl, authBearer, instance.getString("id"));
                deleteBusinessEventInstance(tenantId, apiUrl, exlUrl, authBearer, instance.getString("id"));
            }
            catch (Exception e) {
                System.err.printf("Error deleting Business Event Instance %s : %s\n", instance.getString("id"), e.getMessage());
            }
        });
        // Purge descendants...
        JsonArray results = getBusinessEventDefinitionDescendants(tenantId, apiUrl, exlUrl, authBearer, definitionId);
        results.getValuesAs(JsonObject.class).forEach(descendant -> {
            // Check descendants
            System.out.printf(Locale.US, "Found descendant [%s] for Business Event definition [%s]...\n", descendant.getString("id"), definitionId);
            try {
                purgeBusinessEventInstances(tenantId, apiUrl, exlUrl, authBearer, descendant.getString("id"));
            }
            catch (Exception e) {
                System.err.printf("Error deleting Business Event Instances for descendant %s : %s\n", descendant.getString("id"), e.getMessage());
            }
        });
    }

    private static void stopBusinessEventInstance(String tenantId, String apiUrl, String exlUrl, String authBearer, String id) throws Exception {
        WebClient client = WebClient.create(apiUrl, new ArrayList<Object>() {{
            add(new JsrJsonpProvider());
            add(new JacksonJsonProvider());
        }});
        client = client.path(exlUrl).path("businessEvent/" + id + "/stop");
        configureClient(client);
        client.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
            .header(HttpHeaders.AUTHORIZATION, authBearer).header("X-Tenant-Id", tenantId);
        client.type(MediaType.APPLICATION_JSON);
        JsonObject body = Json.createObjectBuilder().add("reason", "stop").build();
        Response response = client.post(body);
        if (response.getStatus() != 200 && response.getStatus() != 204) {
            // log information
            System.err.printf(Locale.US, "An error occurred stopping Business Event Instance.\n");
            // we received an error status from the server, throw an internal server exception
            throw new Exception("Error getting B.E. Instance: " + response.readEntity(String.class));
        }
        // Wait for the process to stop
        while (true) {
            System.out.printf(Locale.US, ".");
            JsonObject instance = getBusinessEventInstance(tenantId, apiUrl, exlUrl, authBearer, id);
            if (instance.getJsonObject("status").getString("value").equals("Cancelled"))
                break;
            Thread.sleep(1000);
        }
    }

    private static JsonObject getBusinessEventInstance(String tenantId, String apiUrl, String exlUrl, String authBearer, String id) throws Exception {
        WebClient client = WebClient.create(apiUrl, new ArrayList<Object>() {{
            add(new JsrJsonpProvider());
            add(new JacksonJsonProvider());
        }});
        client = client.path(exlUrl).path("businessEvent");
        configureClient(client);
        client.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
            .header(HttpHeaders.AUTHORIZATION, authBearer).header("X-Tenant-Id", tenantId).header("X-Fetch-Options", "process,document");
        client.type(MediaType.APPLICATION_JSON);
        client.query("filter", String.format("id%%20eq%%20'%s'", id));
        Response response = client.get();
        if (response.getStatus() != 200) {
            // log information
            System.err.printf(Locale.US, "An error occurred getting Business Event Instance list.\n");
            // we received an error status from the server, throw an internal server exception
            throw new Exception("Error getting Business Event instance list.");
        }
        // Get response
        return response.readEntity(JsonObject.class).getJsonArray("value").getJsonObject(0);
    }

    private static JsonArray getBusinessEventDefinitionDescendants(String tenantId, String apiUrl, String exlUrl, String authBearer, String definitionId) {
        WebClient client = WebClient.create(apiUrl, new ArrayList<Object>() {{
            add(new JsrJsonpProvider());
            add(new JacksonJsonProvider());
        }});
        client = client.path(exlUrl).path("businessEventDefinition");
        configureClient(client);
        client.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
            .header(HttpHeaders.AUTHORIZATION, authBearer).header("X-Tenant-Id", tenantId).header("X-Fetch-Options", "process,document");
        client.type(MediaType.APPLICATION_JSON);
        String filter = String.format("source/id eq '%s'", definitionId);
        client.query("filter", filter);
        Response response = client.get();
        if (response.getStatus() != 200) {
            // log information
            System.err.printf(Locale.US, "An error occurred getting Business Event Instance list.\n");
            // we received an error status from the server, throw an internal server exception
            throw new RuntimeException("Error getting Business Event instance list.");
        }
        // Get response
        return response.readEntity(JsonObject.class).getJsonArray("value");
    }

    private static void deleteBusinessEventInstance(String tenantId, String apiUrl, String exlUrl, String authBearer, String id) throws Exception {
        WebClient client = WebClient.create(apiUrl, new ArrayList<Object>() {{
            add(new JsrJsonpProvider());
            add(new JacksonJsonProvider());
        }});
        client = client.path(exlUrl).path("businessEvent/" + id);
        configureClient(client);
        client.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
            .header(HttpHeaders.AUTHORIZATION, authBearer).header("X-Tenant-Id", tenantId);
        client.type(MediaType.APPLICATION_JSON);
        Response response = client.delete();
        if (response.getStatus() != 200 && response.getStatus() != 204) {
            // log information
            System.err.printf(Locale.US, "An error occurred deleting Business Event Instance.\n");
            // we received an error status from the server, throw an internal server exception
            throw new Exception("Error getting B.E. Instance: " + response.readEntity(String.class));
        }
    }

    private static JsonObject getBusinessEventInstances(String tenantId, String apiUrl, String exlUrl, String authBearer, String definitionId) throws Exception {
        WebClient client = WebClient.create(apiUrl, new ArrayList<Object>() {{
            add(new JsrJsonpProvider());
            add(new JacksonJsonProvider());
        }});
        client = client.path(exlUrl).path("businessEvent");
        configureClient(client);
        client.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
            .header(HttpHeaders.AUTHORIZATION, authBearer).header("X-Tenant-Id", tenantId).header("X-Fetch-Options", "process,document");
        client.type(MediaType.APPLICATION_JSON);
        client.query("filter", String.format("definition/id%%20eq%%20'%s'", definitionId));
        Response response = client.get();
        if (response.getStatus() != 200) {
            // log information
            System.err.printf(Locale.US, "An error occurred getting Business Event Instance list.\n");
            // we received an error status from the server, throw an internal server exception
            throw new Exception("Error getting Business Event instance list.");
        }
        // Get response
        return response.readEntity(JsonObject.class);
    }

    private static void deleteBusinessEventPackage(String tenantId, String apiUrl, String exlUrl, String authBearer, String uuid, String version) throws Exception {
        // Get package
        JsonObject pkg = getBusinessEventPackage(tenantId, apiUrl, exlUrl, authBearer, uuid, version);
        // Now delete it
        WebClient client = WebClient.create(apiUrl, new ArrayList<Object>() {{
            add(new JsrJsonpProvider());
            add(new JacksonJsonProvider());
        }});
        client = client.path(exlUrl).path("businessEventPackage/" + pkg.getString("id"));
        configureClient(client);
        client.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
            .header(HttpHeaders.AUTHORIZATION, authBearer).header("X-Tenant-Id", tenantId);
        client.type(MediaType.APPLICATION_JSON);
        Response response = client.delete();
        if (response.getStatus() != 200 && response.getStatus() != 204) {
            // log information
            System.err.printf(Locale.US, "An error occurred deleting Business Event Package.\n");
            // we received an error status from the server, throw an internal server exception
            throw new Exception("Error getting B.E. Package");
        }
    }

    private static JsonObject getBusinessEventPackage(String tenantId, String apiUrl, String exlUrl, String authBearer, String uuid, String version) throws Exception {
        WebClient client = WebClient.create(apiUrl, new ArrayList<Object>() {{
            add(new JsrJsonpProvider());
            add(new JacksonJsonProvider());
        }});
        client = client.path(exlUrl).path("businessEventPackage");
        configureClient(client);
        client.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
            .header(HttpHeaders.AUTHORIZATION, authBearer).header("X-Tenant-Id", tenantId);
        client.type(MediaType.APPLICATION_JSON);
        client.query("filter", String.format("identifier%%20eq%%20'%s'%%20and%%20version%%20eq%%20'%s'", uuid, version));
        Response response = client.get();
        if (response.getStatus() != 200) {
            // log information
            System.err.printf(Locale.US, "An error occurred deleting Business Event Definition.\n");
            // we received an error status from the server, throw an internal server exception
            throw new Exception("Error getting B.E. Package");
        }
        JsonObject jresult = response.readEntity(JsonObject.class);
        // Get value
        if (jresult.getJsonArray("value").size() == 0)
            return null;
        if (jresult.getJsonArray("value").size() != 1) {
            System.err.printf(Locale.US, "Invalid number of packages with requested uuid/version. Only one expected and got (%d)\n", jresult.getJsonArray("value").size());
            throw new RuntimeException("Invalid number of packages.");
        }
        return jresult.getJsonArray("value").getJsonObject(0);
    }

    private static void deleteBusinessEventDefinition(String tenantId, String apiUrl, String exlUrl, String authBearer, String id) throws Exception {
        // Delete descendants before, in order to eliminate references.
        JsonArray results = getBusinessEventDefinitionDescendants(tenantId, apiUrl, exlUrl, authBearer, id);
        results.getValuesAs(JsonObject.class).forEach(descendant -> {
            // Check descendants
            System.out.printf(Locale.US, "Found descendant [%s] for Business Event definition [%s]...\n", descendant.getString("id"), id);
            try {
                deleteBusinessEventDefinition(tenantId, apiUrl, exlUrl, authBearer, descendant.getString("id"));
            }
            catch (Exception e) {
                System.err.printf("Error deleting Business Event descendant %s : %s\n", descendant.getString("id"), e.getMessage());
            }
        });
        // Now delete this one...
        WebClient client = WebClient.create(apiUrl, new ArrayList<Object>() {{
            add(new JsrJsonpProvider());
            add(new JacksonJsonProvider());
        }});
        client = client.path(exlUrl).path("businessEventDefinition/" + id);
        configureClient(client);
        client.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
            .header(HttpHeaders.AUTHORIZATION, authBearer).header("X-Tenant-Id", tenantId);
        client.type(MediaType.APPLICATION_JSON);
        Response response = client.delete();
        if (response.getStatus() != 200 && response.getStatus() != 204) {
            // log information
            System.err.printf(Locale.US, "An error occurred deleting Business Event Definition.\n");
            // we received an error status from the server, throw an internal server exception
            throw new Exception("Error getting B.E. Definition:" + response.readEntity(String.class));
        }
    }

    private static JsonObject getBusinessEventDefinition(String tenantId, String apiUrl, String exlUrl, String authBearer, String uuid, String version) throws Exception {
        WebClient client = WebClient.create(apiUrl, new ArrayList<Object>() {{
            add(new JsrJsonpProvider());
            add(new JacksonJsonProvider());
        }});
        client = client.path(exlUrl).path("businessEventDefinition");
        configureClient(client);
        client.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
            .header(HttpHeaders.AUTHORIZATION, authBearer).header("X-Tenant-Id", tenantId).header("X-Fetch-Options", "process,document");
        client.type(MediaType.APPLICATION_JSON);
        client.query("filter", String.format("(identifier eq '%s' or identifier eq '%s:%s') and version eq '%s'", uuid, uuid, version, version));
        Response response = client.get();
        if (response.getStatus() != 200) {
            // log information
            System.err.printf(Locale.US, "An error occurred when uploading the documents.\n");
            // we received an error status from the server, throw an internal server exception
            throw new Exception("Error getting B.E. Definition");
        }
        // Get response
        JsonObject jresult = response.readEntity(JsonObject.class);
        // Get value
        if (jresult.getJsonArray("value").size() == 0)
            return null;
        return jresult.getJsonArray("value").getJsonObject(0);
    }

    private static JsonObject waitForPackageStatus(String tenantId, String apiUrl, String exlUrl, String authBearer, JsonObject jpkg) throws Exception {
        String id = jpkg.getString("id");
        while (true) {
            //String url = apiUrl + "businessEventPackage/" + id;
            WebClient client = WebClient.create(apiUrl, new ArrayList<Object>() {{
                add(new JsrJsonpProvider());
                add(new JacksonJsonProvider());
            }});
            client = client.path(exlUrl).path("businessEventPackage/" + id);
            configureClient(client);
            client.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, authBearer).header("X-Tenant-Id", tenantId).header("X-Fetch-Options", "status");
            client.type(MediaType.APPLICATION_JSON);
            Response response = client.get();
            if (response.getStatus() != 200) {
                // log information
                System.err.printf(Locale.US, "An error occurred when uploading the documents.\n");
                // we received an error status from the server, throw an internal server exception
                throw new Exception("Error getting B.E.P. status");
            }
            // Get response
            JsonObject jresult = response.readEntity(JsonObject.class);
            String status = jresult.getJsonObject("status").getString("value");
            if (status.equals("Error") || status.equals("Success"))
                return jresult;
            // Repeat call.
            Thread.sleep(1000);
        }
    }

    private static JsonObject deployPackage(String tenantId, String apiUrl, String exlUrl, String authBearer, JsonObject jdeploy) {
        //String url = apiUrl + "businessEventPackage";
        WebClient client = WebClient.create(apiUrl, new ArrayList<Object>() {{
            add(new JsrJsonpProvider());
            add(new JacksonJsonProvider());
        }});
        client = client.path(exlUrl).path("businessEventPackage");
        configureClient(client);
        client.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
            .header(HttpHeaders.AUTHORIZATION, authBearer).header("X-Tenant-Id", tenantId);
        client.type(MediaType.APPLICATION_JSON);
        Response response = client.post(jdeploy);
        // check response status
        if (response.getStatus() != 200) {
            // log information
            System.err.printf(Locale.US, "An error occurred when uploading the documents.\n");
            // we received an error status from the server, throw an internal server exception
            try {
                String result = response.readEntity(String.class);
                System.err.printf("Error deplying package: \n%s", result);
            }
            catch (Exception e) {
                // Eat it up.
            }
            return null;
        }
        else {
            JsonObject jresult = response.readEntity(JsonObject.class);
            return jresult;
        }
    }

    private static void configureClient(WebClient client) {
        ClientConfiguration clientConfiguration = WebClient.getConfig(client);
        // add logging interceptors
//        clientConfiguration.getInInterceptors().add(new LoggingInInterceptor());
//        clientConfiguration.getOutInterceptors().add(new LoggingOutInterceptor());
        // http conduit configuration
        HTTPConduit httpConduit = clientConfiguration.getHttpConduit();
        // configure http conduit
        configureHttpConduit(httpConduit);
    }

    private static JsonObject uploadPackage(String tenantId, String apiUrl, String exlUrl, String authBearer, String filename) throws FileNotFoundException {

        //String url = apiUrl + "document/content";
        WebClient client = WebClient.create(apiUrl, new ArrayList<Object>() {{
            add(new JsrJsonpProvider());
            add(new JacksonJsonProvider());
        }});
        client = client.path(exlUrl).path("document/content");
        configureClient(client);
        client.header(HttpHeaders.CONTENT_TYPE, MediaType.WILDCARD)
            .header(HttpHeaders.AUTHORIZATION, authBearer).header("X-Tenant-Id", tenantId);
        // Prepare form
        client.type("multipart/form-data");
        List<Attachment> attachmentList = new ArrayList<Attachment>();
        File f = new File(filename);
        ContentDisposition cd = new ContentDisposition(String.format(Locale.US, "attachment;filename=%s", f.getName()));
        attachmentList.add(new Attachment(f.getName(), new FileInputStream(f), cd));
        // create the multipart
        MultipartBody body = new MultipartBody(attachmentList);
        Response response = client.post(body);
        // check response status
        if (response.getStatus() != 200) {
            // log information
            System.err.printf(Locale.US, "An error occurred when uploading the documents.\n");
            // we received an error status from the server, throw an internal server exception
            return null;
        }
        else {
            JsonArray uploadInfoData = response.readEntity(JsonArray.class);
            JsonObject jresult = uploadInfoData.getJsonObject(0);
            return jresult;
        }
    }

    private static void configureHttpConduit(HTTPConduit httpConduit) {
        // tls client parameters
        TLSClientParameters parameters = httpConduit.getTlsClientParameters();
        if (parameters == null) {
            // create parameters for SSL/TLS
            parameters = new TLSClientParameters();
            // update conduit
            httpConduit.setTlsClientParameters(parameters);
        }
        // check we need to disable hostname verification for SSL
        // log information
        //System.out.printf(Locale.US, "Disabling server hostname verification for SSL/TLS connections.\n");
        // disable certificate CN verification
        parameters.setDisableCNCheck(true);
        // log information
        //System.out.printf(Locale.US, "Disabling Certificate chain validation for SSL/TLS connections.\n");
        // Create a trust manager that does not validate certificate chains
        parameters.setTrustManagers(new TrustManager[]{new TrustServerCertificatesTrustManager()});
    }

    private static String processBasicAuthentication(String username, String password) {
        Base64.Encoder encoder = Base64.getEncoder();
        return encoder.encodeToString((username + ":" + password).getBytes(Charset.forName("US-ASCII")));
    }

    public static String getApplicationToken(String apiEndpoint, String key, String secret) {

        return getApplicationToken(apiEndpoint, processBasicAuthentication(key, secret));
    }

    public static String getApplicationToken(String apiEndpoint, String basicAuthToken) {
        // current user token
        // consumer key and secret
        // create jax-rs client
        WebClient client = WebClient.create(apiEndpoint, new ArrayList<Object>() {{
            add(new JsrJsonpProvider());
            add(new JacksonJsonProvider());
        }});
        client.authorization("Basic " + basicAuthToken);
        configureClient(client);
        client.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON);
        client.type(MediaType.APPLICATION_JSON);
        try {
            // https://api-gateway/token
            client = client.path("token");
            // POST application/x-www-form-urlencoded
            Response response = client.form(new Form("grant_type", "client_credentials"));
            // check response status
            if (response.getStatus() == 200) {
                // get JsonObject
                JsonObject jsonObject = response.readEntity(JsonObject.class);
                // expires in
                int expiresIn = jsonObject.getInt("expires_in");
                // create application token instance
                return jsonObject.getString("access_token");
            }
            else {
                // check we have a JSON response
                if (MediaType.APPLICATION_JSON.compareTo(response.getHeaderString("Content-Type")) == 0) {
                    // read json payload
                    JsonObject payload = response.readEntity(JsonObject.class);
                    // log information
                    System.err.printf(Locale.US, "Cannot obtain client credentials for application consumer key/secret, received HTTP status code: %s, payload: %s", response.getStatus(), payload);
                    // throw exception, TODO: get message from resource file
                    throw new ForbiddenException();
                }
                // throw exception, TODO: get message from resource file
                throw new ForbiddenException();
            }
        }
        finally {
            // close client
            client.close();
        }
    }

    private static JsonObject resolveObjectReferences(JsonObject source) {
        Map<String, JsonObject> refTable = new HashMap<>();
        return resolveObjectReferences(source, refTable);
    }

    private static JsonObject resolveObjectReferences(JsonObject source, Map<String, JsonObject> refTable) {
        JsonObjectBuilder target = Json.createObjectBuilder();
        source.entrySet().forEach(entry -> {
            JsonValue value = entry.getValue();
            target.add(entry.getKey(), processObjectValue(value, refTable));
        });
        JsonObject result = target.build();
        String refId = source.getString("$id", null);
        if (StringUtils.isNoneEmpty(refId))
            refTable.put(refId, result);
        return result;
    }

    private static JsonValue processObjectValue(JsonValue value, Map<String, JsonObject> refTable) {
        if (value.getValueType().equals(JsonValue.ValueType.OBJECT)) {
            // Get refId... if there's one
            JsonObject obj = (JsonObject)value;
            String ref = obj.getString("$ref", null);
            JsonObject tmpobj;
            if (StringUtils.isEmpty(ref)) {
                tmpobj = resolveObjectReferences(obj, refTable);
            }
            else {
                // Search in table
                if (!refTable.containsKey(ref))
                    throw new RuntimeException("Invalid reference: $ref=" + ref);
                tmpobj = refTable.get(ref);
            }
            return tmpobj;
        }
        else if (value.getValueType().equals(JsonValue.ValueType.ARRAY)) {
            JsonArray array = (JsonArray)value;
            JsonArrayBuilder abuilder = Json.createArrayBuilder();
            array.forEach(avalue -> {
                if (avalue.getValueType().equals(JsonValue.ValueType.OBJECT)) {
                    JsonObject obj = (JsonObject)avalue;
                    JsonValue tvalue = processObjectValue(obj, refTable);
                    abuilder.add(tvalue);
                }
            });
            return abuilder.build();
        }
        return value;
    }

}
